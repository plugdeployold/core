<?php

return [
    'menu' => [
        'clinica' => 'Clínica',
        'equipa' => 'Equipa',
        'servicos' => 'Serviços',
        'testemunhos' => 'Testemunhos',
        'contactos' => 'Contactos',
    ],
    'homepage' => [
        'clinica' => [
            'subtitle' => 'A Dentesmalte é uma rede clínica odontológica, especialista em transformar sonhos em realidade.',
            'description' => 'Entrega aos pacientes muito mais que atendimento humanizado, mas um tratamento odontológico de qualidade e acessível. Acredita que um sorriso saudável pode trazer segurança, felicidade, bem-estar, reconhecimento e autoestima para a vida das pessoas.',
        ],
        'equipa' => [
            'subtitle' => 'Somos profissionais dedicados ao seu sorriso',
            'description' => 'A equipa Dentesmalte é constituída por vários clínicos e técnicos que estão em constante formação, acompanhando a mais alta evolução a nível mundial. Conscientes do impacto que a transformação do sorriso e do rosto causam aos nossos pacientes, dedicamos todo o nosso empenho a cada momento que partilhamos com a cada um.',
        ],
        'servicos' => [
            'subtitle' => 'Da prevenção à intervenção complexa.',
            'description' => 'Na Dentesmalte irá encontrar várias opções de tratamento nas mais variadas especialidades dentárias para conseguir o seu sorriso e satisfazer as suas necessidades.',
            'implantes' => 'Implantes',
            'ortodontia' => 'Ortodontia',
            'facetas-coroas' => 'Facetas e Coroas',
            'branqueamento' => 'Branqueamento',
            'cirurgia-plastica' => 'Cirurgia plástica',
            'rejuvenecimento' => 'Rejuvenecimento',
        ],
        'testemunhos' => [
            'subtitle' => 'O que dizem sobre nós',
        ],
        'contactos' => [
            'subtitle' => 'Como nos encontrar',
        ],
    ],
    'pages' => [
        'clinica' => [

        ],
        'equipa' => [

        ],
        'servicos' => [

        ],
        'testemunhos' => [

        ],
        'contactos' => [

        ],
    ],
    'ler-mais' => 'Ler mais',
];
