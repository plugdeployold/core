@extends('basetheme::layouts.master')

@section('stylesheets')
<link rel="icon" sizes="16x16" href="Images/favicon.ico">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500">

<link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="//stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<link rel="stylesheet" href="/core/install/css/style.css">
@endsection

@section('body')

<div class="container requirement" id="requirement">

    <div style="text-align:center; margin-top:50px">
        <img width="350px" src="/installer/Images/logo.svg" />
    </div>


    <div class="initial-display">
        <p>Request Project</p>
    </div>

    @if (session()->get('message_error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ session()->get('message_error') }}</strong>
    </div>
    @endif

    @if (session()->get('message_success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ session()->get('message_success') }}</strong>
    </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-1">
            <div class="card card-default">
                <div class="card-body">
                    <form action="{{ route('core.install.store') }}" method="POST" id="environment-form">
                        @csrf
                        <div class="form-group" id="app_domain">
                            <label for="application_domain" class="required">Project Domain</label>
                            <input required type="text" name="app_domain" id="application_domain" class="form-control" placeholder="example.com" data-validation="required length" data-validation-length="max20">
                        </div>
                        <div class="form-group" id="app_type">
                            <label for="application_type" class="required">App Type</label>
                            <select required name="app_type" id="application_type" class="form-control" data-validation="required">
                                <option value="Eccommerce" selected>Eccommerce</option>
                            </select>
                        </div>
                        <div class="form-group" id="app_plan">
                            <label for="application_plan" class="required">App Solution</label>
                            <select required name="app_plan" id="application_plan" class="form-control" data-validation="required">
                                <option value="Sprout" selected>Sprout - 9,99€</option>
                                <option value="Blossom">Blossom - 19,99€</option>
                                <option value="Garden">Garden - 49,99€</option>
                                <option value="Estate">Estate - 99,99€ </option>
                                <option value="Forest">Forest - 199,99€</option>
                            </select>
                        </div>
                        <div class="form-group" id="app_region">
                            <label for="application_region" class="required">Region</label>
                            <select required name="app_region" id="application_region" class="form-control" data-validation="required">
                                <option value="ams2" selected>Amsterdam</option>
                                <option value="nyc2">New York</option>
                                <option value="sfo1">San Francisco</option>
                                <option value="sgp1">Singapore</option>
                                <option value="lon1">London</option>
                            </select>
                        </div>

                        <div class="text-center"><input type="submit" class="btn btn-primary" id="requirement-check" value="Request" /></div>

                        <?php /*
                        <?php if (!isset($requirements['errors']) && ($phpVersion['supported'] && $composerInstall['composer_install'] == 0)): ?>
                        <div class="text-center"><button type="button" class="btn btn-primary" id="requirement-check">Continue</button></div>
                        <?php elseif (!($phpVersion['supported'] && $composerInstall['composer_install'] == 0)): ?>
                        <div class="text-center"><button type="button" class="btn btn-primary" id="requirements-refresh">Refresh</button></div>
                        <?php endif;?> */ ?>

                        <div style="margin-bottom: 5px; margin-top: 30px; text-align: center; color: #000000">
                            <a href="https://plugdeploy.com/" target="_blank">Plugdeploy</a> a plataform by <a href="https://plugwithus.com/" target="_blank">Plug With Us</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
