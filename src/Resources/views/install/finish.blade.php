@extends('basetheme::layouts.master')

@section('stylesheets')
<link rel="icon" sizes="16x16" href="Images/favicon.ico">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500">

<link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="//stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<link rel="stylesheet" href="/core/install/css/style.css">
@endsection

@section('body')

<div class="container requirement" id="requirement">

    <div style="text-align:center; margin-top:50px">
        <img width="350px" src="/installer/Images/logo.svg" />
    </div>


    <div class="initial-display">
        <p>Request Done ✅</p>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-1">
            <div class="card card-default">
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card card-default">
                                <div class="card-body">
                                    <div class="alert alert-success">
                                        <b>PlugDeploy</b> is successfully requested on your system. Waiting for a confirmation.<br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="" role="toolbar" aria-label="buttons">
                                <button class="btn btn-primary" onclick="finish()">Launch the admin interface</button>
                            </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    /*function finish() {
        next = window.location.href.split("/installer")[0];
        next = next.concat('/admin/login');
        window.location.href = next;
    }*/

</script>


@endsection
