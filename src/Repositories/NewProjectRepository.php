<?php

namespace Plugdeploy\Core\Repositories;

use Illuminate\Support\Str;
use Prettus\Repository\Eloquent\BaseRepository;

class NewProjectRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'Plugdeploy\Core\Contracts\NewProject';
    }

    /**
     * @param array $data
     *
     * @return \Plugdeploy\Core\Contracts\NewProject
     */
    public function create(array $data)
    {
        $newproject = $this->model->create($data);

        return $newproject;
    }

}
