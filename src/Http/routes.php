<?php

use Plugdeploy\Core\Core;

Route::group(['middleware' => ['web']], function () {
    Route::prefix('plugdeploy')->group(function () {
        Route::prefix('core')->group(function () {

            Route::get('/', function () {
                return "PlugDeploy Core Package";
            });;

            Route::get('/greet/{name}', function ($sName) {
                $oGreetr = new Core();
                return $oGreetr->greet($sName);
            });
        });

        Route::prefix('install')->group(function () {

            Route::get('/', 'Plugdeploy\Core\Http\Controllers\Install\InstallController@index')->name('core.install.index');

            Route::post('/createRequest', 'Plugdeploy\Core\Http\Controllers\Install\InstallController@store')->name('core.install.store');

            /*Route::post('/store-project-request', 'Plugdeploy\Core\Http\Controllers\Install\InstallController@storeProjectRequest')->defaults('_config', [
        'redirect' => 'core.install.index',
        ])->name('core.install.store-project-request');*/
        });
    });
});

//
