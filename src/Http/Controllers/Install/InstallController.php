<?php

namespace Plugdeploy\Core\Http\Controllers\Install;

use Illuminate\Routing\Controller as BaseController;
use Plugdeploy\Core\Http\Controllers\NewProject\NewProjectController;

class InstallController extends BaseController
{

    public static function index()
    {
        request()->session()->forget('error');

        return view('core::install.index');
    }

    /**
     * Save all configs of request to install a new project
     *
     */
    public static function store()
    {
        $installation_requirements = array(
            'app_domain' => request()->input('app_domain'),
            'app_type' => request()->input('app_type'),
            'app_plan' => request()->input('app_plan'),
            'app_region' => request()->input('app_region'),
        );

        $error = NewProjectController::store($installation_requirements);

        if ($error) {
            return redirect()->route('core.install.index')->with('message_error', $error);
        } else {
            $success = "Our Project is successfully requested on your system. Waiting for a confirmation.";
            return redirect()->route('core.install.index')->with('message_success', $success);
        }

        /*return response()->json([
        'message' => 'Installation Requirements been created successfully.',
        'data' => $installation_requirements,
        ]);*/

        /*$authorization = "Authorization: Bearer 0414cdda769180a3a33e45bc38ba3012d03be7e64b4a32be24a1661cd3368e02";

        $url = "https://api.digitalocean.com/v2/droplets";

        $post_array = array(
        'name' => "example.com",
        'region' => request()->input('app_region'),
        'size' => "s-1vcpu-1gb",
        'image' => "ubuntu-16-04-x64",
        'ssh_keys' => [
        26337608,
        ],
        'backups' => false,
        'ipv6' => true,
        'user_data' => null,
        'private_networking' => null,
        'volumes' => null,
        'tags' => [
        "web",
        ],
        );

        $request = curl_init();

        curl_setopt($request, CURLOPT_URL, $url);

        curl_setopt($request, CURLOPT_HTTPHEADER, array(
        $authorization,
        'accept: application/json',
        'content-type: application/json',
        ));

        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($request, CURLOPT_POST, 1);

        curl_setopt($request, CURLOPT_POSTFIELDS, json_encode($post_array));

        $response = curl_exec($request);

        curl_close($request);

        if ($response === false) {
        return response()->json(['response' => curl_error($response)]);
        }*/

        //return response()->json(['response' => json_decode($response, true)]);

    }
}
