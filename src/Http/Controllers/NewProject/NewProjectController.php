<?php

namespace Plugdeploy\Core\Http\Controllers\NewProject;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Plugdeploy\Core\Models\NewProject;

class NewProjectController extends Controller
{

    /**
     * Store a new flight in the database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public static function store($installation_requirements)
    {
        $newProject = new NewProject;

        $newProject->app_domain = $installation_requirements['app_domain'];
        $newProject->app_type = $installation_requirements['app_type'];
        $newProject->app_plan = $installation_requirements['app_plan'];
        $newProject->app_region = $installation_requirements['app_region'];

        try {
            $newProject->save();
        } catch (\Illuminate\Database\QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == '1062') {
                return "Domain Name already exists. Choose Another."; // insert query
            }
        }
    }
}
