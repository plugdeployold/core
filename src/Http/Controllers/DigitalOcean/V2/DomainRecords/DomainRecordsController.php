<?php

namespace WebsiteTheme\Http\Controllers\DigitalOcean\V2\DomainRecords;

use Illuminate\Routing\Controller as BaseController;

class DomainRecordsController extends BaseController
{
    /**
     * List All Domain Records
     *
     * @return response
     */
    protected function listAllDomainsRecords($domain)
    {
        $authorization = "Authorization: Bearer 0414cdda769180a3a33e45bc38ba3012d03be7e64b4a32be24a1661cd3368e02";

        $url = "https://api.digitalocean.com/v2/domains/${domain}/records";

        $request = curl_init();

        curl_setopt($request, CURLOPT_URL, $url);

        curl_setopt($request, CURLOPT_HTTPHEADER, array(
            $authorization,
            'accept: application/json',
            'content-type: application/json',
        ));

        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($request);

        curl_close($request);

        if ($response === false) {
            return response()->json(['response' => curl_error($response)]);
        }

        return response()->json(['response' => json_decode($response, true)]);
    }

    /**
     * Create a New Domain Record
     *
     * @return response
     */
    protected function createNewDomainRecord($domain)
    {
        $authorization = "Authorization: Bearer 0414cdda769180a3a33e45bc38ba3012d03be7e64b4a32be24a1661cd3368e02";

        $url = "https://api.digitalocean.com/v2/domains/${domain}/records";

        $post_array = array(
            'type' => "A",
            'name' => "www",
            'data' => "162.10.66.0",
            'priority' => null,
            'port' => null,
            'ttl' => 1800,
            'weight' => null,
            'flags' => null,
            'tag' => null,
        );

        $request = curl_init();

        curl_setopt($request, CURLOPT_URL, $url);

        curl_setopt($request, CURLOPT_HTTPHEADER, array(
            $authorization,
            'accept: application/json',
            'content-type: application/json',
        ));

        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($request, CURLOPT_POST, 1);

        curl_setopt($request, CURLOPT_POSTFIELDS, json_encode($post_array));

        $response = curl_exec($request);

        curl_close($request);

        if ($response === false) {
            return response()->json(['response' => curl_error($response)]);
        }

        return response()->json(['response' => json_decode($response, true)]);
    }
}
