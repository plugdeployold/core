<?php

namespace WebsiteTheme\Http\Controllers\DigitalOcean\V2\Droplets;

use Illuminate\Routing\Controller as BaseController;

class DropletsController extends BaseController
{
    /**
     * Create a New Droplet
     *
     * @return response
     */
    protected function createNewDomainDroplet()
    {
        $authorization = "Authorization: Bearer 0414cdda769180a3a33e45bc38ba3012d03be7e64b4a32be24a1661cd3368e02";

        $url = "https://api.digitalocean.com/v2/droplets";

        $post_array = array(
            'name' => "example.com",
            'region' => "nyc3",
            'size' => "s-1vcpu-1gb",
            'image' => "ubuntu-16-04-x64",
            'ssh_keys' => [
                26337608,
            ],
            'backups' => false,
            'ipv6' => true,
            'user_data' => null,
            'private_networking' => null,
            'volumes' => null,
            'tags' => [
                "web",
            ],
        );

        $request = curl_init();

        curl_setopt($request, CURLOPT_URL, $url);

        curl_setopt($request, CURLOPT_HTTPHEADER, array(
            $authorization,
            'accept: application/json',
            'content-type: application/json',
        ));

        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($request, CURLOPT_POST, 1);

        curl_setopt($request, CURLOPT_POSTFIELDS, json_encode($post_array));

        $response = curl_exec($request);

        curl_close($request);

        if ($response === false) {
            return response()->json(['response' => curl_error($response)]);
        }

        return response()->json(['response' => json_decode($response, true)]);
    }
}
