<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('app_domain')->unique();
            $table->string('app_type');
            $table->string('app_plan');
            $table->string('app_region');
            $table->string('payed')->default(false);
            $table->string('installed')->default(false);
            $table->string('active')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_projects');
    }
}
